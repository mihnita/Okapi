# Changes from M26 to M27

<!-- MACRO{toc} -->

## General

    * Fix resource and memory leaks

## Filters

* XLIFF Filter

    * Added warning when inline code (other than `mrk`)
      has no `id` attribute.
    * Fixed location of `<phase-group>` when
      re-writing.
    * Fixed case where XML declaration was not followed by a
      line-break on output.
    * Added fallback check for TMX values for the `<it>`
      `pos` attribute (error/warning still generated as
      using TMX values in XLIFF is not valid).
    * Added better support for SDLXLIFF
    * Optional parameters for writing out tool element in xliff
      header
    * Fixed [issue #430](https://bitbucket.org/okapiframework/okapi/issues/430) where the ITS namespace declaration and
      version was not added when needed.

* HTML Filter

    * Added the `placeholder` attribute to list of
      translatable attributes in default HTML configuration (for
      HTML5)
    * Fix lower casing of start tags during pre-processing cleanup
    * Upgrade to Jericho 3.4-dev

## Steps

* Rainbow Translation Kit Creation Step

    * Updated XLIFF2 library to 1.0 release.
    * Implemented v2 support for the Transifex packages.

* Rainbow Translation Kit Post-Processing Step

    * Implemented v2 support for the Transifex packages.

## Connectors

* MyMemory Connector

    * Fixed the issue of the return match value being sometimes a
      Double and sometimes a Long.
    * Make Connectors more error tolerant. Continue processing if
      there is an exception on a single text Unit

## Libraries

* XLIFFWriter

    * Added support to output the `coord` attribute (<em>`COORDINATES`</em>
      property on the text container).

* Transifex Library

    * Fixed [issue #427](https://bitbucket.org/okapiframework/okapi/issues/427) where the API v2 was not supported.

* Segmentation Library

    * Fixed [issue #426](https://bitbucket.org/okapiframework/okapi/issues/426) where the part of the text matched by the
      previous rule was not scanned for match in the next rule.
    * Fixed [issue #489](https://bitbucket.org/okapiframework/okapi/issues/489): Added the `okp:treatIsolatedCodesAsWhitespace`
      option to allow the segmenter to treat each isolated code as a
      single whitespace character when applying segmentation rules.

* Verification Library

    * Fixed [issue #418](https://bitbucket.org/okapiframework/okapi/issues/418): the description of the rule is now displayed
      for target-driven error.
    * Improved reading of LQI entries: the ITS type is preserved
      when reading the `okp:lqiType` value.
    * Fixed [issue #442](https://bitbucket.org/okapiframework/okapi/issues/442): Allow flagging blacklist terms in
      substrings.
    * Fixed [issue #400](https://bitbucket.org/okapiframework/okapi/issues/400): Allow flagging blacklist terms in source.

* Parameters editor for Verification Library

    * Fixed [issue #417](https://bitbucket.org/okapiframework/okapi/issues/417): The description of each pattern is now
      preserved when re-ordering the patterns.
    * Fixed [issue #442](https://bitbucket.org/okapiframework/okapi/issues/442): Add option to allow flagging blacklist terms
      in substrings.
    * Fixed [issue #400](https://bitbucket.org/okapiframework/okapi/issues/400): Add option to allow flagging blacklist terms
      in source.
