/*===========================================================================
  Copyright (C) 2009-2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.common;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class compares two zip files containing XML entries to see if they have
 * the same contents. This can be used to compare zip file output with a gold
 * standard zip file.
 */
public class ZipXMLFileCompare {
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	public boolean compareFiles (String out, String gold) {

		if (!new File(out).exists()) {
			LOGGER.trace("ZipCompare:  Output file {} not found.\n", out);
			return false;
		}
		if (!new File(gold).exists()) {
			LOGGER.trace("ZipCompare:  Gold file {} not found.\n", gold);
			return false;
		}

		try (ZipFile outZipFile = new ZipFile(out);
				ZipFile goldZipFile = new ZipFile(gold)){

			if( outZipFile.size() != goldZipFile.size() ){
				LOGGER.trace("Difference in number of files:");
				LOGGER.trace(" out: {}", outZipFile.size());
				LOGGER.trace("gold: {}", goldZipFile.size()+"\n");
				LOGGER.trace(Util.getFilename(out, true));
				return false;
			}

			Map<String, ? extends ZipEntry> outZipMap = outZipFile.stream()
					.collect(Collectors.toMap(ZipEntry::getName, Function.identity()));
			Map<String, ? extends ZipEntry> goldZipMap = goldZipFile.stream()
					.collect(Collectors.toMap(ZipEntry::getName, Function.identity()));

			if( !outZipMap.keySet().equals(goldZipMap.keySet()) ){
				LOGGER.trace("Filenames do not match between the zipfiles\n");
				LOGGER.trace(Util.getFilename(out, true));
				return false;
			}

			boolean failure = false;
			for (String filename: outZipMap.keySet()) {
				ZipEntry oze = outZipMap.get(filename);
				ZipEntry gze = goldZipMap.get(filename);
				if (!compareZipEntries(outZipFile, oze, goldZipFile, gze)) {
					failure = true;
				}
			}
			return !failure;
		} catch (IOException e) {
			LOGGER.trace("ZipCompare: Error comparing zip files.\n", e);
			return false;
		}
	}

	private boolean compareZipEntries(ZipFile outZipFile, ZipEntry oze, ZipFile goldZipFile, ZipEntry gze) {
		if (!oze.getName().toLowerCase().endsWith(".xml")) return true;
		if (!gze.getName().toLowerCase().endsWith(".xml")) return true;

		// some formats have zero byte xml files (openoffice)
		if (oze.getSize() <= 0 && gze.getSize() <= 0) {
			return true;
		}

		boolean same = false;

		File tempFileOut = extractZipEntryToTempFile("~okapi-17_", outZipFile, oze);
		File tempFileGold = extractZipEntryToTempFile("~okapi-18_", goldZipFile, gze);
		if (tempFileOut != null && tempFileGold != null) {
			same = new XMLFileCompare().compareFilesPerLines(tempFileOut.toString(), tempFileGold.toString());
			if (!same) {
				LOGGER.trace("Output and Gold Entry differ\n"
						+ " out: {}::{}\n"
						+ "gold: {}::{}",
						outZipFile.getName(), oze.getName(),
						goldZipFile.getName(), gze.getName());
			}
		}
		FileUtil.deleteFile(tempFileOut.toString());
		FileUtil.deleteFile(tempFileGold.toString());

		return same;
	}

	private File extractZipEntryToTempFile(String prefix, ZipFile zipFile, ZipEntry ze) {
		File tempFile = FileUtil.createTempFile(prefix + Util.getFilename(ze.getName(), true) + "_");

		try (InputStream is = zipFile.getInputStream(ze)) {
			Files.copy(is, tempFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			FileUtil.deleteFile(tempFile.toString());
			LOGGER.trace("Error writing zip entry to temporary file.", e);
			return null;
		}

		return tempFile;
	}
}
