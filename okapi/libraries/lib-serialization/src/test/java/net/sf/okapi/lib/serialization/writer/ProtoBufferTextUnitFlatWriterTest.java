/*===========================================================================
  Copyright (C) 2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.serialization.writer;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.DummyFilter;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.lib.serialization.writer.ProtoBufferTextUnitFlatWriter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ProtoBufferTextUnitFlatWriterTest {
	private static final String SIMPLE_OUTPUT = "textUnits {  id: \"tu1\"  translatable: true  source {    parts {    " +
			"  id: \"0\"      text {        text: \"Source text\"      }      segment: true    }    locale: \"en\"  } " +
			" targets {    key: \"fr\"    value {      parts {        id: \"0\"        text {          text: \"Target " +
			"text\"        }        segment: true      }      locale: \"fr\"    }  }}textUnits {  id: \"tu2\"  " +
			"translatable: true  source {    parts {      id: \"0\"      text {        text: \"Source text 2\"      } " +
			"     segment: true    }    locale: \"en\"  }}";
	private static final String SEGMENTED_OUTPUT = "textUnits {  id: \"tu1\"  translatable: true  source {    parts { " +
			"     id: \"0\"      text {        text: \"First segment for SRC.\"      }      segment: true    }    " +
			"parts {      text {        text: \" \"      }    }    parts {      id: \"1\"      text {        text: " +
			"\"Second segment for SRC\"      }      segment: true    }    segApplied: true    locale: \"en\"  }  " +
			"targets {    key: \"fr\"    value {      parts {        id: \"0\"        text {          text: \"First " +
			"segment for TRG.\"        }        segment: true      }      parts {        text {          text: \" \"  " +
			"      }      }      parts {        id: \"1\"        text {          text: \"Second segment for TRG\"     " +
			"   }        segment: true      }      segApplied: true      locale: \"fr\"    }  }}";
	private static final String CODES_OUTPUT = "textUnits {  id: \"id1\"  translatable: true  source {    parts {     " +
			" id: \"0\"      text {        text: \"Source code \"        codes {          tagType: PLACEHOLDER        " +
			"  id: 1          codeType: \"z\"          data: \"@#$0\"          outerData: \"@#$0\"          position: " +
			"12          isolated: true        }      }      segment: true    }    locale: \"en\"  }  targets {    " +
			"key: \"fr\"    value {      parts {        id: \"0\"        text {          text: \"Target code \"       " +
			"   codes {            tagType: PLACEHOLDER            id: 1            codeType: \"z\"            data: " +
			"\"@#$0\"            outerData: \"@#$0\"            position: 12            isolated: true          }     " +
			"   }        segment: true      }      locale: \"fr\"    }  }}";
	private DummyFilter filter;
	private LocaleId locEN = LocaleId.fromString("en");
	private LocaleId locFR = LocaleId.fromString("fr");

	@Before
	public void setUp() {
		filter = new DummyFilter();
	}

	@Test
	public void testSimpleOutput() {
		String result = rewrite(FilterTestDriver.getEvents(filter, "##def##", locEN, locFR), locFR);
		assertEquals(SIMPLE_OUTPUT, result.replaceAll("[\\r\\n\u001E]", ""));
	}

	@Test
	public void testSegmentedOutput() {
		String result = rewrite(FilterTestDriver.getEvents(filter, "##seg##", locEN, locFR), locFR);
		assertEquals(SEGMENTED_OUTPUT, result.replaceAll("[\\r\\n\u001E]", ""));
	}
	
	@Test
	public void testCodesOutput() {
		String result = rewrite(FilterTestDriver.getEvents(filter, "Source code @#$0\nTarget code @#$0", locEN, locFR), locFR);
		assertEquals(CODES_OUTPUT, result.replaceAll("[\\r\\n\u001E]", ""));
	}

	private String rewrite(ArrayList<Event> list, LocaleId trgLang) {
		ProtoBufferTextUnitFlatWriter writer = new ProtoBufferTextUnitFlatWriter();
		writer.setOptions(trgLang, null);
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		writer.setOutput(output);
		for (Event event : list) {
			writer.handleEvent(event);
		}
		writer.close();
		return output.toString();
	}

}
