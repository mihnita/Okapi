/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.filters.fontmappings.FontMappings;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.List;

interface Formula extends MarkupComponent {
    String F = "f";
    String CALCULATED_COLUMN = "calculatedColumnFormula";
    String TOTALS_ROW = "totalsRowFormula";
    List<XMLEvent> innerEvents();
    MarkupComponent.Context context();
    void readWith(final XMLEventReader reader) throws XMLStreamException;

    class Default implements  Formula {
        private final StartElement startElement;
        private final List<XMLEvent> innerEvents;
        private final MarkupComponent.Context context;
        private EndElement endElement;

        Default(final StartElement startElement, final MarkupComponent.Context context) {
            this(startElement, new ArrayList<>(), context);
        }

        Default(
            final StartElement startElement,
            final List<XMLEvent> innerEvents,
            final MarkupComponent.Context context
        ) {
            this.startElement = startElement;
            this.innerEvents = innerEvents;
            this.context = context;
        }

        @Override
        public List<XMLEvent> innerEvents() {
            return this.innerEvents;
        }

        @Override
        public Context context() {
            return this.context;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
                this.innerEvents.add(e);
            }
        }

        @Override
        public void apply(final FontMappings fontMappings) {
        }

        @Override
        public List<XMLEvent> getEvents() {
            final List<XMLEvent> events = new ArrayList<>(this.innerEvents.size() + 2);
            events.add(this.startElement);
            events.addAll(this.innerEvents);
            events.add(this.endElement);
            return events;
        }
    }
}
