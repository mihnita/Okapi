package net.sf.okapi.filters.openxml;

import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;

import org.assertj.core.api.Assertions;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import net.sf.okapi.common.FileLocation;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class ExcelStyleDefinitionsTest {
	private final XMLFactories xmlfactories = new XMLFactoriesForTest();
	private FileLocation root;

	@Before
	public void setUp() {
		root = FileLocation.fromClass(getClass());
	}

	@Test
	public void argbForegroundColorsDetermined() throws Exception {
		final StyleDefinitions styleDefinitions = new ExcelStyleDefinitions();
		try (final Reader reader = new InputStreamReader(this.root.in("/xlsx_parts/rgb_styles.xml").asInputStream(), OpenXMLFilter.ENCODING.name())) {
			styleDefinitions.readWith(
				new ExcelStyleDefinitionsReader(
					new ConditionalParameters(),
					this.xmlfactories.getEventFactory(),
					this.xmlfactories.getInputFactory().createXMLEventReader(reader)
				)
			);
		}
		Assertions.assertThat(
			styleDefinitions.combinedDifferentialFormatFor(0)
				.fill().pattern().foregroundColor().argb()
		).isEqualTo("");
		Assertions.assertThat(
			styleDefinitions.combinedDifferentialFormatFor(1)
				.fill().pattern().foregroundColor().argb()
		).isEqualTo("FF800000");
		Assertions.assertThat(
			styleDefinitions.combinedDifferentialFormatFor(2)
				.fill().pattern().foregroundColor().argb()
		).isEqualTo("FFFF0000");
		Assertions.assertThat(
			styleDefinitions.combinedDifferentialFormatFor(3)
				.fill().pattern().foregroundColor().argb()
		).isEqualTo("FFFF6600");
		Assertions.assertThat(
			styleDefinitions.combinedDifferentialFormatFor(4)
				.fill().pattern().foregroundColor().argb()
		).isEqualTo("FFFFFF00");
		Assertions.assertThat(
			styleDefinitions.combinedDifferentialFormatFor(5)
				.fill().pattern().foregroundColor().argb()
		).isEqualTo("FFCCFFCC");
		Assertions.assertThat(
			styleDefinitions.combinedDifferentialFormatFor(6)
				.fill().pattern().foregroundColor().argb()
		).isEqualTo("FF008000");
		Assertions.assertThat(
			styleDefinitions.combinedDifferentialFormatFor(7)
				.fill().pattern().foregroundColor().argb()
		).isEqualTo("FF3366FF");
		Assertions.assertThat(
			styleDefinitions.combinedDifferentialFormatFor(8)
				.fill().pattern().foregroundColor().argb()
		).isEqualTo("FF0000FF");
		Assertions.assertThat(
			styleDefinitions.combinedDifferentialFormatFor(9)
				.fill().pattern().foregroundColor().argb()
		).isEqualTo("FF000090");
		Assertions.assertThat(
			styleDefinitions.combinedDifferentialFormatFor(10)
				.fill().pattern().foregroundColor().argb()
		).isEqualTo("FF660066");
	}

	@Test
	public void optionalFillIdOfCellFormatHandled() throws Exception {
		final StyleDefinitions styleDefinitions = new ExcelStyleDefinitions();
		try (final Reader reader = new InputStreamReader(this.root.in("/xlsx_parts/missing_fillId.xml").asInputStream(), OpenXMLFilter.ENCODING.name())) {
			styleDefinitions.readWith(
				new ExcelStyleDefinitionsReader(
					new ConditionalParameters(),
					this.xmlfactories.getEventFactory(),
					this.xmlfactories.getInputFactory().createXMLEventReader(reader)
				)
			);
		}
		Assertions.assertThat(
			styleDefinitions.combinedDifferentialFormatFor(2)
				.fill().pattern().foregroundColor().argb()
		).isEqualTo("");
	}
}
